;; util.el - emacs functions useful for the development of the Dr.
;;           Theobold game
;; Copyright (C) 2017  Christopher Howard

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(require 'cl-lib)

(defun idiv ()
  "Helpful for dividing picolisp strings over multiple lines. Position
point just after the first quotation mark and call this function."
  (interactive)
   (cl-block nil
     (while t
       (let ((orig-col (current-column)))
         (dotimes (i 72)
           (forward-char)
           (when (or
                  (= (char-after) ?\")
                  (= (char-after) ?\n))
             (cl-return)))
         (while (not (char-equal (char-after) ?\s))
           (backward-char))
         (forward-char)
         (insert-char ?\\)
         (insert-char ?\n)
         (insert-char ?\s orig-col)))))

(defun undiv ()
  "Puts a string back together that was split with idiv. Position point
just after the first quotation mark and call this function."
  (interactive)
   (let ((orig-col (current-column)))
     (while (not (char-equal (char-after) ?\"))
       (if (and
            (char-equal (char-after) ?\\)
            (char-equal (char-after (+ (point) 1)) ?\n))
           (delete-char (+ 2 orig-col))
       (forward-char)))))

(defun cleanup ()
  "Helpful for dividing picolisp strings over multiple
lines. Position point just after the first quotation mark and
call this function."
  (interactive)
   (let ((orig-point (point)))
     (undiv)
     (goto-char orig-point)
     (idiv)))
     
