## Makefile - Makefile for building the dr-theobold package
## Copyright (C) 2017  Christopher Howard

## This program is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.

## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.

## You should have received a copy of the GNU General Public License
## along with this program.  If not, see <http://www.gnu.org/licenses/>.

PREFIX=/usr/local

all:
	echo "No compiling necessary."

install:
	echo '#!/usr/bin/env bash' > dr-theobold
	echo pil $(PREFIX)/share/dr-theobold/dr-theobold.l -start-game >> dr-theobold
	cp dr-theobold.l dr-theobold.l.tmp
	sed -i s+tree.l+$(PREFIX)/share/dr-theobold/tree.l+ dr-theobold.l.tmp
	mkdir -p $(PREFIX)/share/dr-theobold
	cp tree.l $(PREFIX)/share/dr-theobold/tree.l
	chmod 644 $(PREFIX)/share/dr-theobold/tree.l
	cp dr-theobold.l.tmp $(PREFIX)/share/dr-theobold/dr-theobold.l
	chmod 644 $(PREFIX)/share/dr-theobold/dr-theobold.l
	mkdir -p $(PREFIX)/bin
	cp dr-theobold $(PREFIX)/bin/dr-theobold
	chmod 755 $(PREFIX)/bin/dr-theobold

uninstall:
	rm -rf $(PREFIX)/share/dr-theobold
	rm -f $(PREFIX)/bin/dr-theobold

clean:
	rm -f dr-theobold.l.tmp
	rm -f dr-theobold

