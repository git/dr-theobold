COPYING INFORMATION FOR THIS FILE

README.txt - the README file for the Dr. Theobold text adventure game
Copyright (C) 2017  Christopher Howard

You may redistribute and/or modify this file under the terms of the
GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any
later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

ABOUT THIS GAME

`Dr. Theobold' is a text adventure game, which means that you play the
game in a terminal, and that you play by reading text in the terminal
and enter commands on the keyboard.

Commands need to be kept as simple as possible to be recognized. E.g.,
you would enter `put water in bottle' and not `put the water in the
bottle'.

Use the `help' command to see a list of available commands. The list
does not include every possible command that can be used, but only the
most common ones. When you find an object, try to think of natural
commands that would be involved in interacting with it: for example,
if you find a cup, try `drink from cup'.

DEPENDENCIES

This game requires the PicoLisp interpreter, which is available under
the MIT/X11 License from <http://software-lab.de/down.html>.  The game
was developed with PicoLisp version 16.12, which is available as a
Debian package in the `Stretch' repository (Debian 9.*), or from
<http://download.savannah.nongnu.org/releases/dr-theobold/>. As the
game uses a very simple subset of PicoLisp functionality, it is likely
that the game will also work well with newer releases of PicoLisp.

STARTING THE GAME

The game can be run from the source code directory. After installing
the picolisp interpreter, enter the source code directory and run the
command

 $ pil dr-theobold.l -start-game

INSTALLATION (GNU/LINUX)

 Enter the source code directory and run the command

 $ sudo make install

 This will install the necessary files to $(PREFIX)/share/dr-theobold
 and $(PREFIX)/bin, where $(PREFIX) is /usr/local by default. You can
 change PREFIX by editing the Makefile script.

Afterwords, you can simply run the command

 $ dr-theobold

to start the game, provided that $(PREFIX)/bin is in your executable
path.
